## Training project to get knowledge about redux-toolkit

## For custom server I use [json-server](https://www.npmjs.com/package/json-server)

to run use: `json-server --watch db.json --port 5000`

## For code testing I use [Sonarqube](https://docs.sonarqube.org/latest/)

to run use:
1 `docker run -d --name sonarqube -e SONAR_ES_BOOTSTRAP_CHECKS_DISABLE=true -p 9000:9000 sonarqube:latest`
2 `sonar-scanner`
