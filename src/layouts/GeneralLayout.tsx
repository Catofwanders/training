import { Pane } from "evergreen-ui";
import { Outlet } from "react-router-dom";

const GeneralLayout = () => {
  return (
    <Pane padding={20}>
      <Outlet />
    </Pane>
  );
};

export default GeneralLayout;
