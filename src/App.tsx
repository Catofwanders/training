import React from "react";
import { Navigate, useRoutes } from "react-router-dom";
import Posts from "routes/Posts";
import GeneralLayout from "layouts/GeneralLayout";
import Main from "routes/Main";
import HeaderNav from "components/HeaderNav";

function App() {
  const mainRoutes = {
    path: "/",
    element: <GeneralLayout />,
    children: [
      { path: "*", element: <Navigate to="/404" /> },
      { path: "/", element: <Main /> },
      { path: "404", element: <p>404</p> },
      { path: "posts", element: <Posts /> },
    ],
  };
  const postRoutes = {
    path: "posts",
    element: <GeneralLayout />,
    children: [
      { path: "*", element: <Navigate to="/404" /> },
      { path: "404", element: <p>404</p> },
    ],
  };
  const routing = useRoutes([mainRoutes, postRoutes]);

  return (
    <div>
      <HeaderNav />
      {routing}
    </div>
  );
}

export default App;
