import fetchData from "store/helpers/fetchData";
import { IPost } from "types/IPost";

export const fetchPosts = fetchData<IPost[]>(
  "post/fetchAll",
  "https://jsonplaceholder.typicode.com/posts"
);
