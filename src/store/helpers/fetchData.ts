import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export default function fetchData<T>(thunkUrl: string, url: string) {
  return createAsyncThunk(thunkUrl, async (_, thunkAPI) => {
    try {
      const response = await axios.get<T>(url);
      return response.data;
    } catch (error: any) {
      return thunkAPI.rejectWithValue(error.message);
    }
  });
}
