import { combineReducers } from "@reduxjs/toolkit";
import { postAPI } from "store/services/postService";

const rootSlice = combineReducers({
  [postAPI.reducerPath]: postAPI.reducer,
});

export default rootSlice;
