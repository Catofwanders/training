import { Menu } from "evergreen-ui";
import { Outlet } from "react-router-dom";

const Main = () => {
  return (
    <>
      <h1>Main page</h1>
      <Menu.Divider />
      <Outlet />
    </>
  );
};

export default Main;
