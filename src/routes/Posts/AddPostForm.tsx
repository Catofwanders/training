import { Button, Dialog, Pane, toaster } from "evergreen-ui";
import { Controller, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

import { Input } from "components/form";
import { postAPI } from "store/services/postService";

interface IAddPostForm {
  isShown: boolean;
  onClose: () => void;
}

const schema = () =>
  yup.object().shape({
    title: yup.string().required("Please write some title"),
    body: yup.string().required("Please write some content"),
  });

const defaultValues = {
  title: "",
  body: "",
};

const AddPostForm = ({ isShown, onClose }: IAddPostForm) => {
  const [createPost] = postAPI.useCreatePostMutation();

  const {
    control,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    mode: "onChange",
    resolver: yupResolver(schema()),
    defaultValues,
  });

  const onFormSubmit = async (values: any) => {
    await createPost(values);
    onClose();
    reset();
    toaster.success("Post created", { duration: 4 });
  };

  const onCloseHandler = (e: any) => {
    e.preventDefault();
    e.stopPropagation();
    onClose();
    reset();
  };

  return (
    <>
      <Dialog
        shouldCloseOnOverlayClick={false}
        isShown={isShown}
        hasHeader={false}
        hasFooter={false}
      >
        <h3>AddPostForm</h3>
        <form onSubmit={handleSubmit(onFormSubmit)}>
          <Controller
            name="title"
            control={control}
            render={({ field: { onChange, value } }) => (
              <Input
                onChange={onChange}
                value={value}
                error={errors?.title?.message}
                label="Title"
                placeholder="Post title"
              />
            )}
          />
          <Controller
            name="body"
            control={control}
            render={({ field: { onChange, value } }) => (
              <Input
                onChange={onChange}
                value={value}
                error={errors?.body?.message}
                label="Content"
                placeholder="Content title"
              />
            )}
          />
          <Pane
            marginTop={10}
            marginBottom={30}
            display="flex"
            alignItems="center"
          >
            <Button marginRight={10} type="reset" onClick={onCloseHandler}>
              Cancel
            </Button>
            <Button type="submit" appearance="primary">
              Create post
            </Button>
          </Pane>
        </form>
      </Dialog>
    </>
  );
};

export default AddPostForm;
