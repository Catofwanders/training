import { Button } from "evergreen-ui";
import styled from "styled-components";
import { IPost } from "types/IPost";

interface IPostItem {
  item: IPost;
  deletePost: (item: IPost) => void;
}

const PostItemStyle = styled("div")`
  max-width: calc(50% - 30px);
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  border: 1px solid rgb(33, 33, 33);
  padding: 10px;
  margin-bottom: 20px;
  p {
    margin: 0 0 10px;
  }
`;

const PostItem = ({ item, deletePost }: IPostItem) => {
  const { title, body } = item;
  return (
    <PostItemStyle style={{}}>
      <div>
        <p>
          <b>{title}</b>
        </p>
        <p>{body}</p>
      </div>
      <Button onClick={() => deletePost(item)}>Delete</Button>
    </PostItemStyle>
  );
};

export default PostItem;
