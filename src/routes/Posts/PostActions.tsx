import { Button, Pane } from "evergreen-ui";

interface IPostActions {
  onCreate: () => void;
}

const PostActions = ({ onCreate }: IPostActions) => {
  return (
    <Pane display="flex" flexWrap="wrap" justifyContent="flex-end">
      <Button onClick={onCreate}>Create Post</Button>
    </Pane>
  );
};

export default PostActions;
