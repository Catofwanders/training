import { useState } from "react";
import { Menu, Pane, Spinner } from "evergreen-ui";

import { postAPI } from "store/services/postService";
import PostItem from "./PostItem";
import AddPostForm from "./AddPostForm";
import PostActions from "./PostActions";

const Posts = () => {
  const [modalShow, setModalShow] = useState(false);

  const { data, isLoading, error } = postAPI.useFetchAllPostsQuery(20);
  const [deletePost] = postAPI.useDeletePostMutation();

  return (
    <div className="layout">
      <Pane
        display="flex"
        alignItems="center"
        flexWrap="wrap"
        justifyContent="space-between"
      >
        <h1>Posts page</h1>
        <PostActions onCreate={() => setModalShow(true)} />
      </Pane>
      <Menu.Divider />
      <h2>Posts list: </h2>
      {isLoading && <Spinner />}

      {error && <li>Something gone wrong</li>}
      <Pane display="flex" flexWrap="wrap" justifyContent="space-between">
        {data?.map((i) => (
          <PostItem key={i.id} item={i} deletePost={deletePost} />
        ))}
      </Pane>
      <AddPostForm isShown={modalShow} onClose={() => setModalShow(false)} />
    </div>
  );
};

export default Posts;
