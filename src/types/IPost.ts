export type IPost = {
  id?: string | number;
  title: string;
  body: string;
};
