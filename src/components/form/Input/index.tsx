import { FormField, TextInput, TextInputProps } from "evergreen-ui";

type IInput = {
  onChange: () => void;
  value: string;
  label: string;
  placeholder?: string;
  type?: TextInputProps["type"];
  error?: string;
};

const Input = ({
  onChange,
  value,
  error,
  label,
  placeholder,
  type,
}: IInput) => {
  return (
    <FormField label={label} paddingBottom={10} validationMessage={error}>
      <TextInput
        type={type}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
      />
    </FormField>
  );
};

export default Input;
