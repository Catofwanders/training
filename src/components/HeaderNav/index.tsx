import { Menu } from "evergreen-ui";
import { NavLink } from "react-router-dom";
import styled from "styled-components";

const MenuGroup = styled("ul")`
  display: flex;
  background-color: rgb(33, 33, 33);
  padding: 10px 20px;
  margin: 0;
  a {
    color: white;
    border-bottom: 1px solid transparent;
    margin-right: 20px;
    text-decoration: none;
    transition: 0.2s border;
    &:hover,
    &.active {
      border-bottom-color: white;
    }
  }
  ul {
  }
  li {
    list-style-type: none;
  }
`;

const HeaderNav = () => {
  return (
    <Menu>
      <MenuGroup>
        <li>
          <NavLink to="/">Main</NavLink>
        </li>
        <li>
          <NavLink to="/posts">Posts</NavLink>
        </li>
      </MenuGroup>
    </Menu>
  );
};

export default HeaderNav;
